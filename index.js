const gpmfExtract = require('gpmf-extract');
const goproTelemetry = require(`gopro-telemetry`);
const aws = require('aws-sdk');
const Airtable = require('airtable');

const fs = require('fs');

const file = fs.readFileSync('./test/test.mp4');
const s3 = new aws.S3({ apiVersion: '2006-03-01' });

const base = new Airtable({
  apiKey: process.env.AIRTABLE_API_KEY,
}).base(process.env.AIRTABLE_BASE_VIDEOLAPS);

const table = base('Videos copy')
const Bucket = "shesfast"
const ID = "GH0TEST"

gpmfExtract(file)
  .then(extracted => {
    goproTelemetry(extracted, { preset: 'gpx' }, telemetry => {
    //   fs.writeFileSync('./out/data.json', JSON.stringify(telemetry));
    // console.log('Telemetry saved as JSON');

    const params = {
        Body: String(telemetry), 
        Bucket, 
        Key: `data/${ID}.gpx`, 
        // ACL: "public-read"
        // Tagging: "key1=value1&key2=value2"
    }

    s3.putObject(params, (err, data) => { //.then(console.log).catch(console.error)
        if(err) return console.error(err)

        // console.log(data)

        const url = s3.getSignedUrl('getObject', { Bucket, Key: `data/${ID}.gpx` });
        // console.log('The URL is', url);

        table.create([
            {
                fields: {
                    ID,
                    Date: extracted.timing.start,
                    link: `https://${Bucket}.s3-us-west-1.amazonaws.com/upload/${ID}.mp4`,
                    // "data": telemetry, // 415 too big
                    Attachments: [
                        {
                            url,
                            // url: `https://${Bucket}.s3-us-west-1.amazonaws.com/data/${ID}.json`,
                            // filename: `${ID}.json`
                        }
                    ]
                }
            }
        ]).then(console.log).catch(console.error)
    })


    });
  })
  .catch(error => console.error(error));
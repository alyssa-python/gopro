const path = require('path');
const gpmfExtract = require('gpmf-extract');
const goproTelemetry = require('gopro-telemetry');
const Airtable = require('airtable');

const aws = require('aws-sdk');
const s3 = new aws.S3({ apiVersion: '2006-03-01' });

const base = new Airtable({
  apiKey: process.env.AIRTABLE_API_KEY,
}).base(process.env.AIRTABLE_BASE_VIDEOLAPS);

const table = base('Videos copy')

module.exports.handler = async (event, context) => {
    const bucket = event.Records[0].s3.bucket.name;
    const key = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, ' '));
    const filename = path.parse(key).name 
    console.log('FILE NAME:', filename);

    const getVideoParams = {
        Bucket: bucket,
        Key: key,
    }; 

    const video = await s3.getObject(getVideoParams).promise();
    console.log('CONTENT TYPE:', video.ContentType);

    const extracted = await gpmfExtract(video.Body)
    console.log(extracted.timing)

    const telemetry = await goproTelemetry(extracted, { preset: 'gpx' })
    // console.log(telemetry)

    const putDataParams = {
        Body: String(telemetry), 
        Bucket: bucket, 
        Key: `data/${filename}.gpx`, 
        // ACL: "public-read"
        // Tagging: "key1=value1&key2=value2"
    }
    const data = await s3.putObject(putDataParams).promise()
    console.log(data)

    const url = s3.getSignedUrl('getObject', { Bucket: bucket, Key: putDataParams.Key  });
    // console.log('The URL is', url);

    const records = await table.create([
        {
            fields: {
                ID: filename,
                Date: extracted.timing.start,
                link: `https://${bucket}.s3-us-west-1.amazonaws.com/${key}`,
                // "data": telemetry,
                Attachments: [
                    {
                        url,
                        // url: `https://${Bucket}.s3-us-west-1.amazonaws.com/data/${ID}.json`,
                        // filename: `${ID}.json`
                    }
                ]
            }
        }
    ])
    console.log(records)

    return url
    // return telemetry["1"].streams.GPS5
}